import uproot
import matplotlib.pyplot as plt
import numpy as np 
import csv 

with uproot.open("out.root") as file:
    
    with open('histogram_data.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)

        headers = []
        
        for i in range(len(file.keys())):
            headers.extend([f'Bin Edge {i}', f'Count {i}'])
        writer.writerow(headers)

        all_data = []
        
        for i in range(len(file.keys())):

            values = file.values()[i].values()
            edges = file.values()[i].axes[0].edges() 

            data_to_write = list(zip(edges[:-1], values))

            all_data.append(data_to_write)
            
        for i in range(len(all_data[0])):
            row = []
            for data in all_data:
                row.extend(data[i])
            writer.writerow(row)

    print("Data has been written to histogram_data.csv.")
