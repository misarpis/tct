import ROOT
import uproot 
import csv 
tct_path = "../TCT_Core/TCTAnalyse.sl"

            
def write_root_file(output):
    ROOT.gSystem.Load(tct_path)

    # Open the ROOT file for writing
    file = ROOT.TFile(output, "RECREATE")

    # Initialize the analysis with a specific measurement file
    tct_object = ROOT.PSTCT("test1", 0, 2)  # Second is time offset, third is data format

    # Get the current directory in the ROOT session
    currentDir = ROOT.gDirectory
    keys = currentDir.GetListOfKeys()
    
    for key in currentDir.GetListOfKeys():
        print(key.GetName())
    #     # Read the object
        obj = key.ReadObj()
        
        obj.Write()  # Write the TH1F object to the file
        
    # # Write all objects to the file and close it
    file.Write()
    file.Close()

import pandas as pd
        
if __name__ == "__main__":
    write_root_file("out.root")
    
