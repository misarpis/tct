import pandas as pd
import matplotlib.pyplot as plt

# Load data from a CSV file
data = pd.read_csv('output.csv')

# Plotting
plt.figure(figsize=(10, 5))
plt.plot(data['Bin Center'], data['Bin Content'], label='Bin Content vs. Bin Center')
# plt.xlabel('Bin Center')
# plt.ylabel('Bin Content')
# plt.title('Histogram Data')
# plt.legend()
plt.grid(True)
plt.show()